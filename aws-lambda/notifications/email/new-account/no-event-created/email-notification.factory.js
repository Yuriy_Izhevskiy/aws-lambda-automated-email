const OneDayPassedCommand = require('./one-day-passed.command')
const ThreeDaysPassedCommand = require('./three-days-passed.command')
const TenDaysPassedCommand = require('./ten-days-passed.command')

class EmailNotificationFactory {
  /**
   * Create command instance
   * @param {Object} options - The options object.
   *
   */
  command({ action = '', data }) {
    switch (action.trim()) {
      case 'one-day-passed':
        return new OneDayPassedCommand(data);
      case 'three-days-passed':
        return new ThreeDaysPassedCommand(data);
      case 'ten-days-passed':
        return new TenDaysPassedCommand(data);
      default:
        throw new Error(`Unknown EmailNotificationFactory action - "${action}".`);
    }
  }
}

module.exports = new EmailNotificationFactory();