const EmailNotificationInterface = require('../../email-notification.interface')

class TenDaysPassedCommand extends EmailNotificationInterface{
  constructor(data) {
    super();
    this.fromData(data)
  }
  
  fromData(data) {
    const { eventContactEmail: email } = data.Email
    const { eventContactFirst } = data.Person
    const { ownerID } = data.Owner
    
    this.name = `${eventContactFirst}`
    this.emailTo = email
    this.ownerID = ownerID
    
    this.setTo({ address: { email, name: this.name }});
  }
  
  prepareData() {
    return {
      to: this.to,
      from: {
        name: 'Pokemon',
        email: `no-reply@pokemon.com`
      },
      transactional: true,
      template_id: 'new-account-no-event-created-10-days',
      substitution_data: {
        user: this.name
      },
      subject: 'Let Us Know What You Need',
    }
  }
  
  dataVerifiedSuccessfully() {
    if(!this.emailTo)  return false
    
    return true
  }
  
  async sendEmail() {
    if (!this.dataVerifiedSuccessfully()) return false
    
    const sendRes = await this._mailerFactory.send(this.prepareData())
    return this.prepareSendResults(sendRes)
  }
  
  prepareSendResults(data) {
    let { results } = data
    if (results) {
      return {
        transmissionID: results.id,
        ownerID: this.ownerID,
        firstName: this.name,
        emailTo: this.emailTo,
      }
    }
    return false
  }
}

module.exports = TenDaysPassedCommand