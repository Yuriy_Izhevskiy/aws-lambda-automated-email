const EmailNotificationInterface = require('../email-notification.interface')

class EmailTwoDaysPassedCommand extends EmailNotificationInterface {
  constructor(data) {
    super();
    this.fromData(data)
  }
  
  fromData(data) {
    const { eventContactEmail: email } = data.Email
    const { eventContactFirst, eventContactLast } = data.Person
    const { eventTitle, regEventID } = data.regEvent
    this.name = `${eventContactFirst}`
    this.emailTo = email
    this.regEventID = regEventID
  
    this.setEventTitle(eventTitle)
    this.setTo({ address: { email, name: this.name }});
  }
  
  prepareData() {
    return {
      to: this.to,
      from: {
        name: 'Pokemonc',
        email: `no-reply@pokemon.com`
      },
      transactional: true,
      template_id: 'cf-event-closes',
      substitution_data: {
        user: this.name,
        eventTitle: this.eventTitle
      },
      subject: `🎉 Congrats on ${this.eventTitle} & Quick Question`,
    }
  }
  
  dataVerifiedSuccessfully() {
    if(!this.emailTo)  return false
    
    return true
  }
  
  async sendEmail() {
    if (!this.dataVerifiedSuccessfully()) return false
    
    const sendRes = await this._mailerFactory.send(this.prepareData())
    return this.prepareSendResults(sendRes)
  }
  
  prepareSendResults(data) {
    let { results } = data
    if (results) {
      return {
        transmissionID: results.id,
        regEventID: this.regEventID,
        firstName: this.name,
        eventTitle: this.eventTitle,
        emailTo: this.emailTo,
      }
    }
    return false
  }
  
}

module.exports = EmailTwoDaysPassedCommand;