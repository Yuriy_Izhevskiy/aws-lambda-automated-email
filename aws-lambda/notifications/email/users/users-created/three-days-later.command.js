const EmailNotificationInterface = require('../../email-notification.interface')

class ThreeDaysLaterCommand extends EmailNotificationInterface{
  
  constructor(data) {
    super();
    this.fromData(data)
  }
  
  fromData(data) {
    const { userContactEmail: email } = data.PersonEmail
    const { userContactFirst } = data.Person
    const { UserID } = data.UserTable
    
    this.firstName = `${userContactFirst}`
    this.emailTo = email
    this.userID = UserID
    
    this.setTo({ address: { email, name: this.firstName }});
  }
  
  prepareData() {
    return {
      to: this.to,
      from: {
        name: 'Pokemon',
        email: `no-reply@pokemon.com`
      },
      transactional: true,
      template_id: 'new-user-created-on-existing-account-3-days-later',
      substitution_data: {
        first_name: this.firstName
      },
      subject: 'Help is Here!',
    }
  }
  
  dataVerifiedSuccessfully() {
    if(!this.emailTo)  return false
    
    return true
  }
  
  async sendEmail() {
    if (!this.dataVerifiedSuccessfully()) return false
    
    const sendRes = await this._mailerFactory.send(this.prepareData())
    return this.prepareSendResults(sendRes)
  }
  
  prepareSendResults(data) {
    let { results } = data
    if (results) {
      return {
        transmissionID: results.id,
        userID: this.userID,
        firstName: this.firstName,
        emailTo: this.emailTo,
      }
    }
    return false
  }
}

module.exports = ThreeDaysLaterCommand