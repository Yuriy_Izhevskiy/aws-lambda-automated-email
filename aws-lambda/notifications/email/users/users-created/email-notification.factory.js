const ThreeDaysLaterCommand = require('./three-days-later.command')

class EmailNotificationFactory {
  /**
   * Create command instance
   * @param {Object} options - The options object.
   *
   */
  command({ action = '', data }) {
    if (action.trim() === 'three-days-later') {
      return new ThreeDaysLaterCommand(data);
    } else {
      throw new Error(`Unknown EmailNotificationFactory action - "${action}".`);
    }
  }
}

module.exports = new EmailNotificationFactory()