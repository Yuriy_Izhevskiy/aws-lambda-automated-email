const EmailTwoDaysPassedCommand = require('./email-two-days-passed.command')

class FirstTimeAeEmailNotificationFactory {
  /**
   * Create command instance
   * @param {Object} options - The options object.
   *
   */
  command({ action = '', data }) {
    switch (action.trim()) {
      case 'two-days-passed':
        return new EmailTwoDaysPassedCommand(data);
      default:
        throw new Error(`Unknown FirstTimeAe action - "${action}".`);
    }
  }
}

module.exports = new FirstTimeAeEmailNotificationFactory();