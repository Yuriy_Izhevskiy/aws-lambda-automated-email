const EmailTwoDaysPassedCommand = require('./email-two-days-passed.command')

class P2pEventClosesEmailNotificationFactory {
  /**
   * Create command instance
   * @param {Object} options - The options object.
   *
   */
  command({ action = '', data }) {
    switch (action.trim()) {
      case 'two-days-passed':
        return new EmailTwoDaysPassedCommand(data);
      default:
        throw new Error(`Unknown P2pEventCloses action - "${action}".`);
    }
  }
}

module.exports = new P2pEventClosesEmailNotificationFactory();