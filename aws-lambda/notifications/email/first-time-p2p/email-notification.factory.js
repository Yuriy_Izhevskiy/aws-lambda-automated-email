const EmailTwoDaysPassedCommand = require('./email-two-days-passed.command')

class EmailNotificationFactory {
  command({ action = '', data }) {
    if (action.trim() === 'two-days-passed') {
      return new EmailTwoDaysPassedCommand(data);
    } else {
      throw new Error(`Unknown EmailNotificationFactory::command action - "${action}".`);
    }
  }
}

module.exports = new EmailNotificationFactory();