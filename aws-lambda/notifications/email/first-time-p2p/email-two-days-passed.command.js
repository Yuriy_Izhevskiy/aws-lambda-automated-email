const EmailNotificationInterface = require('../email-notification.interface')

class EmailTwoDaysPassedCommand extends EmailNotificationInterface {
  constructor(data) {
    super();
    this.fromData(data)
  }
  
  fromData(data) {
    const { eventContactEmail: email } = data.Email
    const { eventContactFirst, eventContactLast } = data.Person
    const { eventTitle, regEventID } = data.regEvent
    this.firstName = `${eventContactFirst}`
    this.emailTo = email
    this.regEventID = regEventID
    
    this.setEventTitle(eventTitle)
    this.setTo({ address: { email, name: this.firstName }});
  }
  
  prepareData() {
    return {
      to: this.to,
      from: {
        name: 'Pokemon',
        email: `no-reply@pokemon.com`
      },
      transactional: true,
      template_id: 'first-time-p2p-2-days-after-event-set-up-completed',
      substitution_data: {
        first_name: this.firstName,
        event_title: this.eventTitle
      },
      subject: `Getting Started with ${this.eventTitle}: Equipping your Participants`,
    }
  }
  
  dataVerifiedSuccessfully() {
    if(!this.emailTo)  return false
    
    return true
  }
  
  async sendEmail() {
    if (!this.dataVerifiedSuccessfully()) return false
    
    const sendRes = await this._mailerFactory.send(this.prepareData())
    return this.prepareSendResults(sendRes)
  }
  
  prepareSendResults(data) {
    let { results } = data
    if (results) {
      return {
        transmissionID: results.id,
        regEventID: this.regEventID,
        firstName: this.firstName,
        eventTitle: this.eventTitle,
        emailTo: this.emailTo,
      }
    }
    return false
  }
}

module.exports = EmailTwoDaysPassedCommand