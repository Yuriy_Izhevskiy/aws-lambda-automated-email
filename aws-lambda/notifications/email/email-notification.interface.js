const mailerFactory = require('../../notification-client/email/mailer.factory')

class EmailNotificationInterface {
  constructor() {
    this._mailerFactory = mailerFactory.service();
  }
  
  fromData() {
    throw new Error(
      "EmailNotificationInterface it`s abstract class. You must override fromData method"
    );
  }
  
  setTo(to = '') {
    this.to = to;
  }
  
  setEventTitle(eventTitle = '') {
    this.eventTitle = eventTitle
  }
  
  prepareData() {
    throw new Error(
      "EmailNotificationInterface it`s abstract class. You must override prepareData method"
    );
  }
  
  sendEmail() {
    throw new Error(
      "EmailNotificationInterface it`s abstract class. You must override sendEmail method"
    );
  }
}

module.exports = EmailNotificationInterface;