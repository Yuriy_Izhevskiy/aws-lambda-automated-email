const EmailTenDaysBeforeDateCommand = require('./email-ten-days-before-date.command')

class EmailNotificationFactory {
  /**
   * Create command instance
   * @param {Object} options - The options object.
   *
   */
  command({ action = '', data }) {
    switch (action.trim()) {
      case 'ten-days-before-date':
        return new EmailTenDaysBeforeDateCommand(data);
      default:
        throw new Error(`Unknown EmailNotificationFactory::command action - "${action}".`);
    }
  }
}

module.exports = new EmailNotificationFactory()