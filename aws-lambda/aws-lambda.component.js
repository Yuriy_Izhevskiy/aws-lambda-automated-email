const Component = require('component').Component

const awsLambda_authenticate = require('./aws_authenticate')
const { AutomatedEmailComponent } = require('./automated-email/automated-email.component')

const AwsLambdaApi = Component(function ({ router }) {
  router.use('/automated-email', awsLambda_authenticate(), AutomatedEmailComponent.routes())
});

module.exports = { AwsLambdaApi }
