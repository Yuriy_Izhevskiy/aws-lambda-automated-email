const EmailSparkPostService = require("./spark-post.service");

/**
 * Class representing a MailerFactory
 * @class
 */
class MailerFactory {
  
  /**
   * Create Service instance
   * @param {Object} options - The options object.
   * @return {SparkPostService} Service instance.
   */
  service(options = {}) {
    return new EmailSparkPostService();
  }
}

module.exports = new MailerFactory();