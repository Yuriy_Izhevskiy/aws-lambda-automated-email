const { Mailer } = require('mailer')
const { ServerConfig, Config } = require("config")
const MailerInterface = require("./mailer.interface");

/**
 * Class representing a SparkPostService
 * @class
 */
class SparkPostService extends MailerInterface {
  /**
   * @constructs SparkPostService
   */
  constructor() {
    super();
    this.mailer = new Mailer(ServerConfig.sparkpost.secret, {
      test_mode: !ServerConfig.is_production()
    });
  }
  
  /**
   * Send emails
   * @param data
   * @returns {Promise<Response>}
   */
  send(data) {
    return this.mailer.send(data)
  }
}

module.exports = SparkPostService;