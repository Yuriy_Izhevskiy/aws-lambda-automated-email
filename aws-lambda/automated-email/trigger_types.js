const EVENT_TRIGGER_TYPES = {
  'AeEventCloses': {
    key: 'AeEventCloses_CongratsAndSurvey',
    description: '2 days have passed since AE event date'
  },
  'P2pEventCloses': {
    key: 'P2pEventCloses_CongratsAndSurvey',
    description: '2 days have passed since P2P event date'
  },
  'FirstTimeAE_2DaysAfterSetupComplete' : {
    key: 'FirstTimeAE_2DaysAfterSetupComplete',
    description: '2 days have passed since AE Event Setup Complete'
  },
  'FirstTimeP2P_2d_AfterSetupComplete': {
    key: 'FirstTimeP2P_2d_AfterSetupComplete',
    description: '2 days have passed since P2P Event Setup Complete'
  },
  'FirstTimeAeP2p_10DaysBeforeDate': {
    key: 'FirstTimeAeP2p_10DaysBeforeDate',
    description: '10 days until AE or P2P Event date'
  },
  'NewAccount_NoEventCreated_1Day': {
    key: 'NewAccount_NoEventCreated_1Day',
    description : '1 day has passed since new account was created and no events have been created'
  },
  'NewAccount_NoEventCreated_3Days': {
    key: 'NewAccount_NoEventCreated_3Days',
    description: '3 days have passed since a new account was created and no event has been created'
  },
  'NewAccount_NoEventCreated_10Days': {
    key: 'NewAccount_NoEventCreated_10Days',
    description: '10 days have passed since a new organization account is created and no event has been created'
  },
  'CF_Event_Closes_2Days': {
    key: 'CF_Event_Closes_2Days',
    description: '2 days have passed since CF event date.  Do NOT send to on-going campaigns'
  },
  'FirstTime_P2P_EventSetUpBegun': {
    key: 'FirstTime_P2P_EventSetUpBegun',
    description: 'step 1 in P2P event setup is marked as complete'
  },
  'NewUserCreatedOnExistAcc_3DaysLater': {
    key: 'NewUserCreatedOnExistAcc_3DaysLater',
    description: '3 days have passed since a new user created their login on an existing account'
  },
}


const triggers_by_resource_type = {
  event: {
    task: 'automated_email',
    key: 'triggerType',
    choices: EVENT_TRIGGER_TYPES
  }
  
}

module.exports = { triggers_by_resource_type }