const { Moment } = require('ms-core/moment')

function currentDate() {
  return Moment().format("YYYY-MM-DD HH:mm:ss");
}

function subtractDate(count = 0, key = 'seconds') {
  return Moment().subtract(count, key).format("YYYY-MM-DD HH:mm:ss");
}

function addDate(count = 0, key = 'seconds') {
  return Moment().add(count, key).format("YYYY-MM-DD HH:mm:ss");
}

function addDateToGiven(data, count = 0, key = 'seconds') {
  return Moment(data).add(count, key).format("YYYY-MM-DD HH:mm:ss");
}

const isEmpty = (arr) => !Array.isArray(arr) || !arr.length;

const onlyUnique = (array = []) => {
  return array.filter(function(value, index) {
    return array.indexOf(value) == index
  })
}
/**
 * prepare event contact IDs
 * @param eventsData
 * @returns {[]}
 */
function prepareEventContactsIds(eventsData = []) {
  const generalContactPersonIDs = [];
  
  eventsData.map(function(data) {
    const { regEvent: { GeneralContactPersonID, ContactPersonID } } = data
    if (
      (typeof GeneralContactPersonID === "number" && typeof ContactPersonID === "number") &&
      GeneralContactPersonID != ContactPersonID
    ) {
      generalContactPersonIDs.push(GeneralContactPersonID)
    }
  })
  return generalContactPersonIDs
}

module.exports = {
  currentDate,
  subtractDate,
  addDate,
  addDateToGiven,
  isEmpty,
  prepareEventContactsIds,
  onlyUnique
};
