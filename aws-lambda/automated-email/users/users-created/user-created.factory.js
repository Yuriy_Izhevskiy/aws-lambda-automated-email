const ThreeDaysService = require('./three-days/three-days.service')
const ThreeDaysRepo = require('./three-days/three-days.repo')

class UserCreatedFactory {
  service(instance = '') {
    if (instance.trim() === 'three-days-later') {
      return new ThreeDaysService(this.repo('three-days-later'));
    } else {
      throw new Error(`Unknown UserCreatedFactory::service instance - "${instance}".`);
    }
  }
  
  repo(instance = '') {
    if (instance.trim() === 'three-days-later') {
      return new ThreeDaysRepo();
    } else {
      throw new Error(`Unknown UserCreatedFactory::repo instance - "${instance}".`);
    }
  }
}

module.exports = new UserCreatedFactory()