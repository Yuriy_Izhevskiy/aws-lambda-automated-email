const Component = require('component').Component
const UserCreatedFactory = require('./user-created.factory')

const UserCreatedComponent = Component(function ({ router }) {
  router.get('/three-days-later', three_days_later)
})

function* three_days_later() {
  const results = yield UserCreatedFactory.service('three-days-later').runTrigger()
  
  this.ok({...results, 'three_days_later': 'three_days_later'})
}

module.exports = { UserCreatedComponent }