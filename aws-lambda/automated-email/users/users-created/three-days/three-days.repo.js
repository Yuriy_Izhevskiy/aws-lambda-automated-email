const { LegacyDB } = require('ms-shared/legacy-db')
const OldLegacyDB = require("ms-shared/old-legacy-db")


const { UserQuery } = require('../../mixin_query')
/**
 * Class representing a ThreeDaysRepo
 * @class
 */
class ThreeDaysRepo {
  
  /**
   * Create AwsLambdaTaskResultsAutomatedEmail object
   * @param {Object} dto - The value object.
   * @return {Promise} Promise object represents the response.
   */
  async create(dto) {
    return LegacyDB.knex('AwsLambdaTaskResultsAutomatedEmail')
      .insert({...dto})
  }
  
  /**
   *
   * @param triggerType - The value object
   * @returns {Promise<*>}
   */
  async getAllByTriggerTypeFromAwsLambdaTaskResults(options = {}, triggerType = {}) {
    const { startDate, endDate } = options
    
    return LegacyDB.knex('AwsLambdaTaskResultsAutomatedEmail')
      .select('userID', 'createdAt')
      .where({ triggerType, status: 1 })
      .whereBetween(OldLegacyDB.raw('createdAt'), [startDate, endDate])
  }
  
  async getNewUsersCreatedOnExistingAccount(options = {}, exclude = []) {
    const { startDate, endDate } = options
  
    return OldLegacyDB('PokemonCity')
      .options({ nestTables: true })
      .use(UserQuery)
      .select(
        'PokemonCity.ID AS UserID',
        'Charmander.OwnerID AS OwnerID',
        'Person.FirstName AS userContactFirst',
        'Person.LastName AS userContactLast',
        'PersonEmail.Email AS userContactEmail'
      )
      .with_user_object()
      .with_object_owner()
      .with_user_person()
      .leftJoin('UserRoleMap', function () {
        this.on('UserRoleMap.UserID', '=', 'PokemonCity.ID')
          .andOn('UserRoleMap.RoleID', '=', LegacyDB.knex.raw("(select max(RoleID) from UserRoleMap where UserTable.ID = UserRoleMap.UserID and RoleID != 19)"))
      })
      .with_user_can_receive_invoices()
      .with_person_email()
      .where({
        'Charmander.Deleted': 0,
        'Charmander.Committed': 1,
        'PokemonCity.IsPublic': 0,
        'PokemonCity.UserStatusID': 2,
      })
      .whereNull('PokemonCity.LatestLoginDate')
      .user_has_desired_roles()
      .exclude_users(exclude)
      .whereBetween(OldLegacyDB.raw('Charmander.CreatedDate'), [startDate, endDate])
  }
  
}

module.exports = ThreeDaysRepo