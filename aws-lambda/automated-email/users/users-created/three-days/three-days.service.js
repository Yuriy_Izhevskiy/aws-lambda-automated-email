const { triggers_by_resource_type } = require('../../../trigger_types')
const { NewUserCreatedOnExistAcc_3DaysLater } = triggers_by_resource_type.event.choices

const {
  currentDate, subtractDate, addDateToGiven, isEmpty
} = require('../../../utils')

const EmailNotificationFactory = require('../../../../notifications/email/users/users-created/email-notification.factory')

/**
 * Class representing a ThreeDaysService
 * @class
 */
class ThreeDaysService {
  
  /**
   * @constructs ThreeDaysService
   * @param repo
   */
  constructor(repo) {
    this._repo = repo
    this.trigger = NewUserCreatedOnExistAcc_3DaysLater.key.trim()
  }
  
  async insertData(results) {
    return Promise.all(
      results.map(async (data) => {
        if (data) {
          const preparedData = this.prepareDataForInsertData(data)
          await this._repo.create(preparedData)
        }
      })
    );
  }
  
  /**
   * prepare data for inserting data into a table
   * @param data
   * @returns {{transmissionID: *, createdAt: *, triggerType: (*|string), regEventID: *, status: number}}
   */
  prepareDataForInsertData(data) {
    const { transmissionID, userID } = data
    return {
      triggerType: this.trigger,
      transmissionID: transmissionID,
      userID: userID,
      status: 1,
      createdAt: currentDate()
    }
  }
  
  /**
   *
   * @param factoryRequest
   * @returns {Promise<boolean|*>}
   */
  async sendEmail(factoryRequest) {
    const emailProvider = EmailNotificationFactory.command(factoryRequest);
    return emailProvider.sendEmail();
  }
  
  /**
   * start sending Emails
   * @param eventsData
   * @returns {Promise<unknown[]>}
   */
  async startSendingEmails(usersData = []) {
    return Promise.all(
      usersData.map(async data => this.sendEmail({ action: 'three-days-later', data } ))
    )
  }
  
  async runTrigger() {
    const AwsLambdaData = await this.getAwsLambdaDataFromTable()
    const newUsersCreated = await this.getNewUsersCreatedOnExistingAccount(AwsLambdaData)
  
    if (isEmpty(newUsersCreated)) {
      return this.createMessageToResponseIfEmptyData(AwsLambdaData)
    }
    
    const mailingResult = await this.startSendingEmails(newUsersCreated)
    await this.insertData(mailingResult)
  
    return {
      message: isEmpty(mailingResult) ? 'No data for mailing' : 'Sending Emails was successful',
      data: mailingResult
    }
  }
  
  /**
   * Create a message to reply if there is no data for mailing
   * @param eventsData
   * @param AwsLambdaData
   * @returns {boolean|{message: string}}
   */
  createMessageToResponseIfEmptyData(AwsLambdaData) {
    const eventsDataMess = "1. New User Created on Existing Account - 3 days later: [empty];"
    const awsLambdaDataMess = !isEmpty(AwsLambdaData)
      ? `2. Emails have already been sent before. Check the table 'AwsLambdaTaskResultsAutomatedEmail', triggerType = ${this.trigger}`
      : ""
    
    return {
      message: `${eventsDataMess} ${awsLambdaDataMess}`
    }
  }
  
  async getNewUsersCreatedOnExistingAccount(exclude = []) {
    const timeOptions = {
      daysPassedInHours: 72,
      timeIntervalInHours: 3
    }
    const startDate = subtractDate( timeOptions.daysPassedInHours + timeOptions.timeIntervalInHours, 'hours');
    const endDate = addDateToGiven(startDate, timeOptions.timeIntervalInHours, 'hours');
  
    return this._repo.getNewUsersCreatedOnExistingAccount({ startDate, endDate }, exclude)
    
  }
  
  /**
   * get data from table AwsLambdaTaskResultsAutomatedEmail
   * @returns {Promise<*>}
   */
  async getAwsLambdaDataFromTable() {
    const startDate = subtractDate(3, 'hours');
    const endDate = currentDate();
    
    return this._repo.getAllByTriggerTypeFromAwsLambdaTaskResults({ startDate, endDate }, this.trigger)
  }
  
}

module.exports = ThreeDaysService