const Component = require('component').Component

const { UserCreatedComponent } = require('./users-created/user-created.component')

const UsersComponent = Component(function ({ router }) {
  router.use('/users-created', UserCreatedComponent.routes())
})

module.exports = { UsersComponent }