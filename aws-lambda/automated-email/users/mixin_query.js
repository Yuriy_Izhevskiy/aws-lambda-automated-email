const Query = require("query")
const { isEmpty } = require('../utils')
const DESIRED_ROLES = [1, 2, 3, 16, 17, 18, 19, 20, 21]

const UserQuery = Query({
  exclude_users(exclude = []) {
    if (!isEmpty(exclude)) {
      this.whereNotIn('PokemonCity.ID', exclude.map(a => a.userID))
    }
  },
  with_person_email() {
    this.leftJoin('Email as PersonEmail', function () {
      this.on('PokemonCity.PersonID', '=', 'PersonEmail.PersonID')
        .andOn('PersonEmail.DefaultEmail', '=', 1)
    })
  },
  with_user_can_receive_invoices() {
    this.leftJoin('UserRoleMap as UserCanReceiveInvoices', function () {
      this.on('UserCanReceiveInvoices.UserID', '=', 'PokemonCity.ID')
        .andOn('UserCanReceiveInvoices.RoleID', '=', 19)
    })
  },
  with_object_owner() {
    this.join('Owner', 'Owner.ID', 'Charmander.OwnerID')
  },
  with_user_object() {
    this.join('Charmander', 'Charmander.ID', 'PokemonCity.ObjectID')
  },
  with_user_person() {
    this.join('Person', 'PokemonCity.PersonID', 'Person.ID')
  },
  user_has_desired_roles() {
    this.whereIn('UserRoleMap.RoleID', DESIRED_ROLES)
  },
})

module.exports = {
  UserQuery
}