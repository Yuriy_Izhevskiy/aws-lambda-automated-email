const Component = require('component').Component

const { AaEventClosesComponent } = require('./ae-event-closes')
const { P2PEventClosesComponent } = require('./p2p-event-closes')
const { FirstTimeAeComponent } = require('./first-time-ae')
const { FirstTimeAeP2pComponent } = require('./first-time-ae-p2p')
const { FirstTimeP2pComponent } = require('./first-time-p2p')
const { CfEventClosesComponent } = require('./cf-event-closes')

//0.0.0.0:3000/aws-lambda-api/automated-email/new-account/no-event-created/three-days 2h
//0.0.0.0:3000/aws-lambda-api/automated-email/new-account/no-event-created/ten-days   2h
//0.0.0.0:3000/aws-lambda-api/automated-email/users/users-created/three-days-later    2h
const EventsComponent = Component(function ({ router }) {
  router.use('/ae-event-closes', AaEventClosesComponent.routes())//0.0.0.0:3000/aws-lambda-api/automated-email/events/ae-event-closes       1h
  router.use('/p2p-event-closes', P2PEventClosesComponent.routes())//0.0.0.0:3000/aws-lambda-api/automated-email/events/p2p-event-closes    1h
  router.use('/cf-event-closes', CfEventClosesComponent.routes())//0.0.0.0:3000/aws-lambda-api/automated-email/events/cf-event-closes       1h
  router.use('/first-time-ae', FirstTimeAeComponent.routes())//0.0.0.0:3000/aws-lambda-api/automated-email/events/first-time-ae              2h
  router.use('/first-time-ae-p2p', FirstTimeAeP2pComponent.routes())//0.0.0.0:3000/aws-lambda-api/automated-email/events/first-time-ae-p2p   2h
  router.use('/first-time-p2p', FirstTimeP2pComponent.routes())//0.0.0.0:3000/aws-lambda-api/automated-email/events/first-time-p2p           2h
})

module.exports = { EventsComponent }