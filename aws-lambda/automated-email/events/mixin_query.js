const Query = require("query")
const { isEmpty } = require('../utils')

const BillEventTypeIds = {
  p2p: [4, 5, 11, 12],
  attendance: [1, 2, 3, 7, 8, 9, 10],
  crowdfunding: [6]
}
const Product = {
  ae: 1,
  fe: 2
}

const EventQuery = Query({
  exclude_events(exclude = []) {
    if (!isEmpty(exclude)) {
      this.whereNotIn('Caterpie.ID', exclude.map(a => a.regEventID))
    }
  },
  include_events(include = []) {
    if (!isEmpty(include)) {
      this.whereIn('Caterpie.ID', include.map(a => a.regEventID))
    }
  },
  for_person(ids = []) {
    if (!isEmpty(ids)) {
      this.whereIn('Person.ID', ids)
    }
  },
  with_start_date() {
    this.select('CaterpieDisplay.EventDate AS eventDate')
      .leftJoin('CaterpieDisplay', function () {
        this.on('Caterpie.ID', '=', 'CaterpieDisplay.EventID')
          .andOn('CaterpieDisplay.IsStartDate', '=', 1)
      })
  },
  with_billing_and_product() {
    this.leftJoin('Ekans', 'Caterpie.ID', 'Ekans.regEventID')
    this.leftJoin('EkansType', 'Ekans.billEventTypeID', 'EkansType.ID')
  },
  with_reg_event_tracker() {
    this.select('CaterpieTracker.updatedAt AS eventSetupUpdated')
      .leftJoin('CaterpieTracker', 'Caterpie.ID', 'CaterpieTracker.regEventID')
  },
  with_default_contact_email_for_coordinator() {
    this.select('Email.Email AS eventContactEmail')
      .leftJoin('Email', function () {
        this.on('Caterpie.ContactPersonID', '=', 'Email.PersonID')
          .andOn('Email.DefaultEmail', '=', 1)
      })
  },
  with_default_contact_email_for_contacts() {
    this.select('Email.Email AS eventContactEmail')
      .leftJoin('Email', function () {
        this.on('Caterpie.GeneralContactPersonID', '=', 'Email.PersonID')
          .andOn('Email.DefaultEmail', '=', 1)
      })
  },
  with_person_profile_for_coordinator() {
    this.select('Person.FirstName AS eventContactFirst', 'Person.LastName AS eventContactLast')
      .leftJoin('Person', 'Caterpie.ContactPersonID', 'Person.ID')
  },
  with_person_profile_for_contacts() {
    this.select('Person.FirstName AS eventContactFirst', 'Person.LastName AS eventContactLast')
      .leftJoin('Person', 'Caterpie.GeneralContactPersonID', 'Person.ID')
  },
  where_active_event() {
    this.where('Caterpie.IsActive', 1)
  },
  where_no_empty_email() {
    this.whereNotNull('Email.Email')
      .where('Email.Email', '!=', '')
  },
  with_object_owner() {
    this.select('Owner.Name AS OwnerName')
      .join('Owner', 'Owner.ID', 'Charmander.OwnerID')
  },
  required_p2p_bill_event_type() {
    this.whereIn('Ekans.billEventTypeID', BillEventTypeIds.p2p)
  },
  required_cf_bill_event_type() {
    this.whereIn('Ekans.billEventTypeID', BillEventTypeIds.crowdfunding)
  },
  required_attendance_or_p2p_product() {
    this.whereIn('Ekans.billEventTypeID', BillEventTypeIds.attendance)
      .orWhereIn('Ekans.billEventTypeID', BillEventTypeIds.p2p)
  },
  required_attendance_product() {
    this.where('EkansType.billEventProductID', Product.ae)
  },
  is_event_setup_complete() {
    this.where({
      'CaterpieTracker.SetupEventDetails': 1,
      'CaterpieTracker.SetupPreferences': 1,
      'CaterpieTracker.SetupDesign': 1,
      'CaterpieTracker.SetupRegistrationForm': 1,
      'CaterpieTracker.SetupWE-All': 1
    })
  },
  is_event_setup_complete_p2p() {
    this.where({
      'CaterpieTracker.SetupEventDetails': 1,
      'CaterpieTracker.SetupPreferences': 1,
      'CaterpieTracker.SetupDesign': 1,
      'CaterpieTracker.SetupFEPreferences': 1,
      'CaterpieTracker.SetupRegistrationForm': 1,
      'CaterpieTracker.SetupWE-All': 1
    })
  }
})

module.exports = {
  EventQuery
}