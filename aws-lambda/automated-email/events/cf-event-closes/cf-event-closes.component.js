const Component = require("component").Component
const CfEventClosesFactory = require('./cf-event-closes.factory')

const service = CfEventClosesFactory.service()

const CfEventClosesComponent = Component(function ({ router }) {
  router.get('/', cf_event_closes)
})

function* cf_event_closes() {
  const results = yield service.runTrigger()
  this.ok({...results})
}

module.exports = { CfEventClosesComponent }