const { triggers_by_resource_type } = require('../../trigger_types')
const { CF_Event_Closes_2Days } = triggers_by_resource_type.event.choices
const {
  currentDate, subtractDate, addDateToGiven, isEmpty, prepareEventContactsIds
} = require('../../utils')

const CfEventClosesEmailNotificationFactory = require('../../../notifications/email/cf-event-closes/email-notification.factory')
/**
 * Class representing a CfEventClosesService
 * @class
 */
class CfEventClosesService {
  /**
   * @constructs CfEventClosesService
   * @param repo
   */
  
  constructor(repo) {
    this._repo = repo
    this.triggerCfEventCloses2Days = CF_Event_Closes_2Days.key.trim()
  }
  
  /**
   * insert data in table
   * @param results
   * @returns {Promise<unknown[]>}
   */
  async insertData(results) {
    return Promise.all(
      results.map(async (data) => {
        if (data) {
          const preparedData = this.prepareDataForInsertData(data)
          await this._repo.create(preparedData)
        }
      })
    );
  }
  
  /**
   * prepare data for inserting data into a table
   * @param data
   * @returns {{transmissionID: *, createdAt: *, triggerType: (*|string), regEventID: *, status: number}}
   */
  prepareDataForInsertData(data) {
    const { transmissionID, regEventID } = data
    return {
      triggerType: this.triggerCfEventCloses2Days,
      transmissionID: transmissionID,
      regEventID: regEventID,
      status: 1,
      createdAt: currentDate()
    }
  }
  
  /**
   *
   * @param factoryRequest
   * @returns {Promise<boolean|*>}
   */
  async sendEmail(factoryRequest) {
    const emailProvider = CfEventClosesEmailNotificationFactory.command(factoryRequest);
    return emailProvider.sendEmail();
  }
  
  /**
   * start sending Emails
   * @param eventsData
   * @returns {Promise<unknown[]>}
   */
  async startSendingEmails(eventsData) {
    return Promise.all(
      eventsData.map(async data => this.sendEmail({ action: 'two-days-passed', data } ))
    )
  }
  
  async runTrigger() {
    const AwsLambdaData = await this.getAwsLambdaDataFromTable()
    const eventsDataAsEventCoordinator = await this.getEventsDataAsEventCoordinator(AwsLambdaData)

    const eventsDataAsEventContacts = await this.getEventsDataAsEventContacts(
      eventsDataAsEventCoordinator,
      { includeEvents: eventsDataAsEventCoordinator.map((x) => ( x.regEvent )) }
    )
    
    const combineEventsData = eventsDataAsEventCoordinator.concat(eventsDataAsEventContacts)
  
    if (isEmpty(combineEventsData)) {
      return this.createMessageToResponseIfEmptyData(AwsLambdaData)
    }
    
    let mailingResult = await this.startSendingEmails(combineEventsData)
   
    await this.insertData(mailingResult)
  
    return {
      message: isEmpty(mailingResult) ? 'No data for mailing' : 'Sending Emails was successful',
      data: mailingResult
    }
  }
  
  createMessageToResponseIfEmptyData(AwsLambdaData) {
    const eventsDataMess = "1. Get events Data: [empty];"
    const awsLambdaDataMess = !isEmpty(AwsLambdaData)
      ? `2. Emails have already been sent before. Check the table 'AwsLambdaTaskResultsAutomatedEmail', triggerType = ${this.triggerP2pEventCloses}`
      : ""
    
    return {
      message: `${eventsDataMess} ${awsLambdaDataMess}`
    }
  }
  
  /**
   * get events and related data for the Event Coordinator
   * @param exclude - exclude events that are stored in the table: AwsLambdaTaskResultsAutomatedEmail
   * @returns {Promise<*>}
   */
  async getEventsDataAsEventCoordinator(exclude = []) {
    const timeOptions = {
      daysPassedInHours: 48,
      timeIntervalInHours: 2
    };
    const previousStart = subtractDate( timeOptions.daysPassedInHours + timeOptions.timeIntervalInHours, 'hours');
    const previousEnd = addDateToGiven(previousStart, timeOptions.timeIntervalInHours, 'hours');
    
    return this._repo.getEventsAsEventCoordinator({ previousStart, previousEnd }, exclude)
  }
  
  /**
   * get events and related data for the Event Contacts
   * @param eventCoordinator
   * @param include
   * @returns {Promise<void|*|Array>}
   */
  async getEventsDataAsEventContacts(eventCoordinator = [], include = {}) {
    if (isEmpty(eventCoordinator)) return []
    
    const generalContactPersonIDs = prepareEventContactsIds(eventCoordinator)
    
    if (!isEmpty(generalContactPersonIDs)) {
      include.includePersonIds = generalContactPersonIDs
      return this._repo.getEventsAsEventContacts(include)
    }
    return []
  }
  
  /**
   * get data from table AwsLambdaTaskResultsAutomatedEmail
   * @returns {Promise<*>}
   */
  async getAwsLambdaDataFromTable() {
    const startDate = subtractDate(3, 'hours');
    const endDate = currentDate();
    
    return this._repo.getAllByTriggerTypeFromAwsLambdaTaskResults({ startDate, endDate }, this.triggerCfEventCloses2Days)
  }
  
}

module.exports = CfEventClosesService