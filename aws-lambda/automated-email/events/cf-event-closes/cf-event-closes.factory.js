const CfEventClosesService = require('./cf-event-closes.service')
const CfEventClosesRepo = require('./cf-event-closes.repo')

class CfEventClosesFactory {
  
  service(options = {}) {
    return new CfEventClosesService(this.repo())
  }
  
  repo(options = {}) {
    return new CfEventClosesRepo()
  }
  
}

module.exports = new CfEventClosesFactory()