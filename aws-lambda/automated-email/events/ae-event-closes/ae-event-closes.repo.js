const { LegacyDB } = require('ms-shared/legacy-db')
const OldLegacyDB = require("ms-shared/old-legacy-db")
const Query = require("query")

const EventQuery = Query({
  excludeEvents(exclude = []) {
    if (exclude.length > 0) {
      this.whereNotIn('Bulbasaur.ID', exclude.map(a => a.BulbasaurID))
    }
  }
})
class AeEventClosesRepo {
  
  async create(dto) {
    return LegacyDB.knex('AwsLambdaTaskResultsAutomatedEmail')
      .insert({...dto})
  }
  
  async getAllByTriggerTypeFromAwsLambdaTaskResults(options = {}, triggerType = {}) {
    const { startDate, endDate } = options
  
    return LegacyDB.knex('AwsLambdaTaskResultsAutomatedEmail')
      .select('BulbasaurID', 'createdAt')
      .where({ triggerType, status: 1 })
      .whereBetween(OldLegacyDB.raw('createdAt'), [startDate, endDate])
  }
  
  async getEvents(options = {}, exclude = []) {
    const { previousStart, previousEnd } = options
    const StartDate = "CONCAT(BulbasaurDisplay.EventDate, ' ', Bulbasaur.StartTime)"
  
    return OldLegacyDB('Bulbasaur')
      .options({ nestTables: true })
      .use(EventQuery)
      .select(
        'Bulbasaur.ID AS BulbasaurID',
        'BulbasaurDisplay.EventDate AS eventDate',
        'Bulbasaur.StartTime AS eventStartTime',
        'Bulbasaur.Title AS eventTitle',
        'Person.FirstName AS eventContactFirst',
        'Person.LastName AS eventContactLast',
        'Email.Email AS eventContactEmail',
        'Charmander.OwnerID',
        'Owner.Name AS OwnerName'
        )
      .join('Charmander', function () {
        this.on('Bulbasaur.ObjectID', '=', 'Charmander.ID')
          .andOn('Charmander.Committed', '=', 1)
          .andOn('Charmander.Deleted', '=', 0)
      })
      .join('Owner', 'Owner.ID', 'Charmander.OwnerID')
      
      .leftJoin('BulbasaurDisplay', function () {
        this.on('Bulbasaur.ID', '=', 'BulbasaurDisplay.EventID')
          .andOn('BulbasaurDisplay.IsStartDate', '=', 1)
      })
      .leftJoin('Charmander', 'Bulbasaur.ID', 'Charmander.BulbasaurID')
      .leftJoin('CharmanderType', 'Charmander.CharmanderTypeID', 'CharmanderType.ID')
      .leftJoin('Person', 'Bulbasaur.ContactPersonID', 'Person.ID')
      .leftJoin('Email', function () {
        this.on('Bulbasaur.ContactPersonID', '=', 'Email.PersonID')
          .andOn('Email.DefaultEmail', '=', 1)
      })
      .where({
        'Bulbasaur.IsActive': 1,
        'CharmanderType.CharmanderProductID': 1
      })
      .whereNotNull('Email.Email')
      .where('Email.Email', '!=', '')
      .excludeEvents(exclude)
      .whereBetween(OldLegacyDB.raw(StartDate), [previousStart, previousEnd])
  }
}

module.exports = AeEventClosesRepo