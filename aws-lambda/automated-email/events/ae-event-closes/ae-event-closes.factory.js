const AeEventClosesService = require('./ae-event-closes.service')
const AeEventClosesRepo = require('./ae-event-closes.repo')

class AeEventClosesFactory {
  
  service(options = {}) {
    return new AeEventClosesService(this.repo())
  }
  
  repo(options = {}) {
    return new AeEventClosesRepo()
  }
}

module.exports = new AeEventClosesFactory()