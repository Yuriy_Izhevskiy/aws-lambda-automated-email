const { triggers_by_resource_type } = require('../../trigger_types')
const { AeEventCloses } = triggers_by_resource_type.event.choices
const { currentDate, subtractDate, addDateToGiven, isEmpty } = require('../../utils')
const AeEventClosesEmailNotificationFactory = require('../../../notifications/email/ae-event-closes/email-notification.factory')

class AeEventClosesService {
  
  constructor(repo) {
    this._repo = repo
    this.triggerAeEventCloses = AeEventCloses.key.trim()
  }
  
  async insertData(results) {
    return Promise.all(
      results.map(async (data) => {
          if (data) {
            const preparedData = this.prepareDataForInsertData(data)
            await this._repo.create(preparedData)
          }
        })
    );
  }
  
  prepareDataForInsertData(data) {
    const { transmissionID, regEventID } = data
    return {
      triggerType: this.triggerAeEventCloses,
      transmissionID: transmissionID,
      regEventID: regEventID,
      status: 1,
      createdAt: currentDate()
    }
  }
  
  async sendEmail(factoryRequest) {
    const emailProvider = AeEventClosesEmailNotificationFactory.command(factoryRequest);
    return emailProvider.sendEmail();
  }
  
  async startSendingEmails(eventsData) {
    return Promise.all(
      eventsData.map(async data => this.sendEmail({ action: 'two-days-passed', data } ))
    )
  }
  
  async runTriggerTwoDaysPassed() {
    const AwsLambdaData = await this.getAwsLambdaDataFromTable()
    const eventsData = await this.getEventsDataFromTable(AwsLambdaData)
  
    if (isEmpty(eventsData)) {
      return this.createMessageToResponseIfEmptyData(AwsLambdaData)
    }
    
    let mailingResult = await this.startSendingEmails(eventsData)
    await this.insertData(mailingResult)
  
    return {
      message: isEmpty(mailingResult) ? 'No data for mailing' : 'Sending Emails was successful',
      data: mailingResult
    }
  }
  
  /**
   * Create a message to reply if there is no data for mailing
   * @param eventsData
   * @param AwsLambdaData
   * @returns {boolean|{message: string}}
   */
  createMessageToResponseIfEmptyData(AwsLambdaData) {
    const eventsDataMess = "1. Get events Data: [empty];"
    const awsLambdaDataMess = !isEmpty(AwsLambdaData)
      ? `2. Emails have already been sent before. Check the table 'AwsLambdaTaskResultsAutomatedEmail', triggerType = ${this.triggerAeEventCloses}`
      : ""
  
    return {
      message: `${eventsDataMess} ${awsLambdaDataMess}`
    }
  }
  
  async getEventsDataFromTable(exclude = []) {
    const previousStart = subtractDate(50, 'hours');
    const previousEnd = addDateToGiven(previousStart,2, 'hours');
    return this._repo.getEvents({ previousStart, previousEnd }, exclude)
  
  }
  
  async getAwsLambdaDataFromTable() {
    const endDate = currentDate();
    const startDate = subtractDate(3, 'hours');
    return this._repo.getAllByTriggerTypeFromAwsLambdaTaskResults({ startDate, endDate }, this.triggerAeEventCloses)
  }
  
}

module.exports = AeEventClosesService