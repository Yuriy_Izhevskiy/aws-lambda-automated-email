const Component = require("component").Component
const AeEventClosesFactory = require('./ae-event-closes.factory')

const service = AeEventClosesFactory.service()

const AaEventClosesComponent = Component(function ({ router }) {
  router.get('/', ae_event_closes)
})

function* ae_event_closes() {
  const results = yield service.runTriggerTwoDaysPassed()
  this.ok({...results})
}

module.exports = { AaEventClosesComponent }