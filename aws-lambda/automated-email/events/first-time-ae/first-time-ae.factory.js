const FirstTimeAeService = require('./first-time-ae.service')
const FirstTimeAeRepo = require('./first-time-ae.repo')

class FirstTimeAeFactory {
  service(options = {}) {
    return new FirstTimeAeService(this.repo())
  }
  
  repo(options = {}) {
    return new FirstTimeAeRepo()
  }
}

module.exports = new FirstTimeAeFactory()