const Component = require("component").Component
const FirstTimeAeFactory = require('./first-time-ae.factory')

const service = FirstTimeAeFactory.service()

const FirstTimeAeComponent = Component(function ({ router }) {
  router.get('/', first_time_ae)
})

function* first_time_ae() {
  const results = yield service.runTriggerTwoDaysPassed()
  this.ok({...results})
}

module.exports = { FirstTimeAeComponent }