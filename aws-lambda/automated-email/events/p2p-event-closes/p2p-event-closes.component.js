const Component = require("component").Component
const P2pEventClosesFactory = require('./p2p-event-closes.factory')

const service = P2pEventClosesFactory.service()

const P2PEventClosesComponent = Component(function ({ router }) {
  router.get('/', p2p_event_closes)
})

function* p2p_event_closes() {
  const results = yield service.runTriggerTwoDaysPassed()
  this.ok({...results})
}

module.exports = { P2PEventClosesComponent }