const P2pEventClosesService = require('./p2p-event-closes.service')
const P2pEventClosesRepo = require('./p2p-event-closes.repo')

class P2pEventClosesFactory {
  
  service(options = {}) {
    return new P2pEventClosesService(this.repo())
  }
  
  repo(options = {}) {
    return new P2pEventClosesRepo()
  }
  
}

module.exports = new P2pEventClosesFactory()