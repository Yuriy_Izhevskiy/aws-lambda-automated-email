const { LegacyDB } = require('ms-shared/legacy-db')
const OldLegacyDB = require("ms-shared/old-legacy-db")
const { EventQuery } = require('../mixin_query')

/**
 * Class representing a P2pEventClosesRepo
 * @class
 */
class P2pEventClosesRepo {
 
  /**
   * Create AwsLambdaTaskResultsAutomatedEmail object
   * @param {Object} dto - The value object.
   * @return {Promise} Promise object represents the response.
   */
  async create(dto) {
    return LegacyDB.knex('AwsLambdaTaskResultsAutomatedEmail')
      .insert({...dto})
  }
  
  /**
   *
   * @param {Object} options - The value object
   * @param triggerType - The value object
   * @returns {Promise<*>}
   */
  async getAllByTriggerTypeFromAwsLambdaTaskResults(options = {}, triggerType = {}) {
    const { startDate, endDate } = options
    
    return LegacyDB.knex('AwsLambdaTaskResultsAutomatedEmail')
      .select('BulbasaurID', 'createdAt')
      .distinct('BulbasaurID')
      .where({ triggerType, status: 1 })
      .whereBetween(OldLegacyDB.raw('createdAt'), [startDate, endDate])
  }
  
  /**
   *
   * @param options
   * @returns {Promise<*|void>}
   */
  async getEventsAsEventContacts(include = {}) {
    const { includeEvents = [], includePersonIds = [] } = include
    return OldLegacyDB('Bulbasaur')
      .options({ nestTables: true })
      .use(EventQuery)
      .select(
        'Bulbasaur.ID AS BulbasaurID',
        'Bulbasaur.GeneralContactPersonID AS GeneralContactPersonID',
        'Bulbasaur.ContactPersonID AS ContactPersonID',
        'Bulbasaur.StartTime AS eventStartTime',
        'Bulbasaur.Title AS eventTitle',
        'Charmander.OwnerID'
      )
      .with_active_object({ for_table: 'Bulbasaur' })
      .with_object_owner()
      .with_start_date()
      .with_person_profile_for_contacts()
      .with_default_contact_email_for_contacts()
      .where_active_event()
      .where_no_empty_email()
      .include_events(includeEvents)
      .for_person(includePersonIds)
  }
  
  /**
   *
   * @param options
   * @param exclude
   * @returns {Promise<*>}
   */
  async getEventsAsEventCoordinator(options = {}, exclude = []) {
    const { previousStart, previousEnd } = options
    const StartDate = "CONCAT(BulbasaurDisplay.EventDate, ' ', Bulbasaur.StartTime)"
  
    return OldLegacyDB('Bulbasaur')
      .options({ nestTables: true })
      .use(EventQuery)
      .select(
        'Bulbasaur.ID AS BulbasaurID',
        'Bulbasaur.GeneralContactPersonID AS GeneralContactPersonID',
        'Bulbasaur.ContactPersonID AS ContactPersonID',
        'Bulbasaur.StartTime AS eventStartTime',
        'Bulbasaur.Title AS eventTitle',
        'Charmander.OwnerID'
      )
      .with_active_object({ for_table: 'Bulbasaur' })
      .with_object_owner()
      .with_start_date()
      .with_billing_and_product()
      .with_person_profile_for_coordinator()
      .with_default_contact_email_for_coordinator()
      .where_active_event()
      .required_p2p_bill_event_type()
      .where_no_empty_email()
      .exclude_events(exclude)
      .whereBetween(OldLegacyDB.raw(StartDate), [previousStart, previousEnd])
  }
  
}

module.exports = P2pEventClosesRepo