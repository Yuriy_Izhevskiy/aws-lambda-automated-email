const FirstTimeAeP2pService = require('./first-time-ae-p2p.service')
const FirstTimeAeP2pRepo = require('./first-time-ae-p2p.repo')

class FirstTimeAeP2pFactory {
  service(options = {}) {
    return new FirstTimeAeP2pService(this.repo())
  }
  
  repo(options = {}) {
    return new FirstTimeAeP2pRepo()
  }
}

module.exports = new FirstTimeAeP2pFactory()