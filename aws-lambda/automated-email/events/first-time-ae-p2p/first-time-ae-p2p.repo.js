const { LegacyDB } = require('ms-shared/legacy-db')
const OldLegacyDB = require("ms-shared/old-legacy-db")
const { EventQuery } = require('../mixin_query')

/**
 * Class representing a FirstTimeAeP2pRepo
 * @class
 */
class FirstTimeAeP2pRepo {
  
  /**
   * Create AwsLambdaTaskResultsAutomatedEmail object
   * @param {Object} dto - The value object.
   * @return {Promise} Promise object represents the response.
   */
  async create(dto) {
    return LegacyDB.knex('AwsLambdaTaskResultsAutomatedEmail')
      .insert({...dto})
  }
  
  /**
   *
   * @param {Object} options - The value object
   * @param triggerType - The value object
   * @returns {Promise<*>}
   */
  async getAllByTriggerTypeFromAwsLambdaTaskResults(triggerType = {}) {
    return LegacyDB.knex('AwsLambdaTaskResultsAutomatedEmail')
      .select('BulbasaurID', 'createdAt')
      .distinct('BulbasaurID')
      .where({ triggerType, status: 1 })
      .whereNotNull('BulbasaurID')
  }
  
  async getEventsDataByUserWhoCreatedEvent() {
    let createdEventsCount = OldLegacyDB('Bulbasaur')
      .options({ nestTables: true })
      .use(EventQuery)
      .select(
        'Bulbasaur.ID AS BulbasaurID',
        'Bulbasaur.GeneralContactPersonID as GeneralContactPersonID',
        'Bulbasaur.ContactPersonID as ContactPersonID',
        'Bulbasaur.StartTime AS eventStartTime',
        'Bulbasaur.Title AS eventTitle',
        'Person.FirstName AS eventContactFirst',
        'Person.LastName AS eventContactLast',
        'Email.Email AS eventContactEmail',
        'Charmander.*',
        'Owner.Name AS OwnerName'
      )
      .join('Charmander', function () {
        this.on('Bulbasaur.ObjectID', '=', 'Charmander.ID')
          .andOn('Charmander.Committed', '=', 1)
          .andOn('Charmander.Deleted', '=', 0)
      })
      .join('Owner', 'Owner.ID', 'Charmander.OwnerID')
      .leftJoin('Charmander', 'Bulbasaur.ID', 'Charmander.BulbasaurID')
      .leftJoin('CharmanderType', 'Charmander.CharmanderTypeID', 'CharmanderType.ID')
      .leftJoin('Person', 'Charmander.CreatedPersonID', 'Person.ID')
      .leftJoin('BulbasaurTracker', 'Bulbasaur.ID', 'BulbasaurTracker.BulbasaurID')
      .leftJoin('Email', function () {
        this.on('Person.ID', '=', 'Email.PersonID')
          .andOn('Email.DefaultEmail', '=', 1)
      })
      .required_attendance_or_p2p_product()
      .whereNotNull('Email.Email')
      .where('Email.Email', '!=', '')
      .groupBy('Bulbasaur.ID')
      .as('Temp')
    
    return LegacyDB.knex.queryBuilder()
      .select('*')
      .count('* AS CreatedEventsCount')
      .from(createdEventsCount)
      .groupBy("Temp.CreatedPersonID")
  }
  
  async getEventsAsEventCoordinator(options = {}, include = {}) {
    const { startDate, endDate } = options
    const { includeEvents = [] } = include
    const StartDate = "CONCAT(BulbasaurDisplay.EventDate, ' ', Bulbasaur.StartTime)"
    
    return OldLegacyDB('Bulbasaur')
      .options({ nestTables: true })
      .use(EventQuery)
      .select(
        'Bulbasaur.ID AS BulbasaurID',
        'Bulbasaur.GeneralContactPersonID AS GeneralContactPersonID',
        'Bulbasaur.ContactPersonID AS ContactPersonID',
        'Bulbasaur.Title AS eventTitle'
      )
      .with_active_object({ for_table: 'Bulbasaur' })
      .with_object_owner()
      .with_start_date()
      .with_billing_and_product()
      .with_person_profile_for_coordinator()
      .with_default_contact_email_for_coordinator()
      .where_no_empty_email()
      .include_events(includeEvents)
      .whereBetween(OldLegacyDB.raw(StartDate), [startDate, endDate])
  }
  
  /**
   * @param options
   * @returns {Promise<void>}
   */
  async getEventsAsEventContacts(include = {}) {
    const { includeEvents = [], includePersonIds = [] } = include
    return OldLegacyDB('Bulbasaur')
      .options({ nestTables: true })
      .use(EventQuery)
      .select(
        'Bulbasaur.ID AS BulbasaurID',
        'Bulbasaur.GeneralContactPersonID AS GeneralContactPersonID',
        'Bulbasaur.ContactPersonID AS ContactPersonID',
        'Bulbasaur.Title AS eventTitle'
      )
      .with_active_object({ for_table: 'Bulbasaur' })
      .with_object_owner()
      .with_billing_and_product()
      .with_person_profile_for_contacts()
      .with_default_contact_email_for_contacts()
      .where_no_empty_email()
      .include_events(includeEvents)
      .for_person(includePersonIds)
  }
  
}

module.exports = FirstTimeAeP2pRepo