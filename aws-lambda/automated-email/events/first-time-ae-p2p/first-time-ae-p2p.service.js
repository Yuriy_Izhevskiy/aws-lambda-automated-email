const { triggers_by_resource_type } = require('../../trigger_types')
const { FirstTimeAeP2p_10DaysBeforeDate } = triggers_by_resource_type.event.choices
const {
  currentDate, addDate, addDateToGiven, isEmpty, prepareEventContactsIds
} = require('../../utils')

const EmailNotificationFactory = require('../../../notifications/email/first-time-ae-p2p/email-notification.factory')

/**
 * Class representing a FirstTimeAeP2pService
 * @class
 */
class FirstTimeAeP2pService {
  
  /**
   * @constructs FirstTimeAeP2pService
   * @param repo
   */
  constructor(repo) {
    this._repo = repo
    this.triggerFirstTimeAeP2p_10DaysBeforeDate = FirstTimeAeP2p_10DaysBeforeDate.key.trim()
  }
  
  /**
   * insert data in table
   * @param results
   * @returns {Promise<unknown[]>}
   */
  async insertData(results) {
    return Promise.all(
      results.map(async (data) => {
        if (data) {
          const preparedData = this.prepareDataForInsertData(data)
          await this._repo.create(preparedData)
        }
      })
    );
  }
  
  /**
   * prepare data for inserting data into a table
   * @param data
   * @returns {{transmissionID: *, createdAt: *, triggerType: (*|string), regEventID: *, status: number}}
   */
  prepareDataForInsertData(data) {
    const { transmissionID, regEventID } = data
    return {
      triggerType: this.triggerFirstTimeAeP2p_10DaysBeforeDate,
      transmissionID: transmissionID,
      regEventID: regEventID,
      status: 1,
      createdAt: currentDate()
    }
  }
  
  /**
   *
   * @param factoryRequest
   * @returns {Promise<boolean|*>}
   */
  async sendEmail(factoryRequest) {
    const emailProvider = EmailNotificationFactory.command(factoryRequest);
    return emailProvider.sendEmail();
  }
  
  /**
   * start sending Emails
   * @param eventsData
   * @returns {Promise<unknown[]>}
   */
  async startSendingEmails(eventsData) {
    return Promise.all(
      eventsData.map(async data => this.sendEmail({ action: 'ten-days-before-date', data } ))
    )
  }
  
  async runTriggerTenDaysBeforeDate() {
    const AwsLambdaData = await this.getAwsLambdaDataFromTable()
    const excludeAwsLambdaEvents = this.getExcludedAwsLambdaEvents(AwsLambdaData)
  
    //created_by
    const eventsDataByUserWhoCreatedEvent = await this.getEventsDataByUserWhoCreatedEvent()
    const firstEvent = this.getFirstEventIfExists(eventsDataByUserWhoCreatedEvent, excludeAwsLambdaEvents)
  
    if (isEmpty(firstEvent)) {
      return this.createMessageToResponseIfEmptyData(AwsLambdaData)
    }
  
    //contact_person
    const includeEventsForCoordinator = {
      includeEvents: firstEvent.map((x) => ( x.regEvent ))
    }
    const eventsDataAsEventCoordinator = await this.getEventsDataAsEventCoordinator(includeEventsForCoordinator)
   
    //coordinator
    const includeEventsForContacts = {
      includeEvents: eventsDataAsEventCoordinator.map((x) => ( x.regEvent ))
    }
    const eventsDataAsEventContacts = await this.getEventsDataAsEventContacts(eventsDataAsEventCoordinator, includeEventsForContacts)
  
    //ready array 'created by'
    const preparedUserWhoCreated = this.prepareUserWhoCreatedEventForSendingEmails(eventsDataAsEventCoordinator, firstEvent)
    const combineEventsData = eventsDataAsEventCoordinator.concat(eventsDataAsEventContacts, preparedUserWhoCreated)
  
    const mailingResult = await this.startSendingEmails(combineEventsData)
    await this.insertData(mailingResult)
  
    return {
      message: isEmpty(mailingResult) ? 'No data for mailing' : 'Sending Emails was successful',
      data: mailingResult
    }
  }
  
  /**
   * Create a message to reply if there is no data for mailing
   * @param eventsData
   * @param AwsLambdaData
   * @returns {boolean|{message: string}}
   */
  createMessageToResponseIfEmptyData(AwsLambdaData) {
    const eventsDataMess = "1. Get first event if exists: [empty];"
    const awsLambdaDataMess = !isEmpty(AwsLambdaData)
      ? `2. Emails have already been sent before. Check the table 'AwsLambdaTaskResultsAutomatedEmail', triggerType = ${this.triggerFirstTimeAeP2p_10DaysBeforeDate}`
      : ""
    
    return {
      message: `${eventsDataMess} ${awsLambdaDataMess}`
    }
  }
  
  /**
   * get events and related data for the Event Contacts
   * @param eventCoordinator
   * @returns {Promise<Array|*>}
   */
  async getEventsDataAsEventContacts(eventCoordinator = [], include = {}) {
    if (isEmpty(eventCoordinator)) return []
    
    const generalContactPersonIDs = prepareEventContactsIds(eventCoordinator)
   
    if (!isEmpty(generalContactPersonIDs)) {
      include.includePersonIds = generalContactPersonIDs
      return this._repo.getEventsAsEventContacts(include)
    }
    return []
  }
  
  getExcludedAwsLambdaEvents(events = []) {
    if (isEmpty(events)) return []
    return events.map(a => a.regEventID)
  }
  
  /**
   * get events and related data for the Event Coordinator
   * @param exclude - exclude events that are stored in the table: AwsLambdaTaskResultsAutomatedEmail
   * @returns {Promise<*>}
   * 10 days until AE or P2P Event date
   */
  async getEventsDataAsEventCoordinator(include = {}) {
    
    const startDate = addDate(240, 'hours');
    const endDate = addDateToGiven(startDate,4, 'hours');
    
    return this._repo.getEventsAsEventCoordinator({ startDate, endDate }, include)
  }
  
  async getEventsDataByUserWhoCreatedEvent() {
    return this._repo.getEventsDataByUserWhoCreatedEvent()
  }
  
  getFirstEventIfExists(events = [], excludeEvents = []) {
    return this.prepareCreatedEvents(events, excludeEvents)
  }
  
  prepareCreatedEvents(events = [], excludeEvents = []) {
    if (isEmpty(events)) return []
    let array = [];
    
    events.map(function (data) {
      const {
        CreatedEventsCount = '', eventContactEmail, eventContactFirst, eventContactLast, eventTitle, regEventID,
        GeneralContactPersonID, ContactPersonID, ID, OwnerID, CreatedPersonID
      } = data
      if (CreatedEventsCount == 1 && !excludeEvents.includes(regEventID)) {
        const prepare = {
          regEvent: { eventTitle, regEventID, GeneralContactPersonID, ContactPersonID },
          Object: { ID, OwnerID, CreatedPersonID },
          Person: { eventContactFirst, eventContactLast},
          Email: { eventContactEmail },
        }
        array.push(prepare)
      }
    })
    return array
  }
  
  /**
   * get data from table AwsLambdaTaskResultsAutomatedEmail
   * @returns {Promise<*>}
   */
  async getAwsLambdaDataFromTable() {
    return this._repo.getAllByTriggerTypeFromAwsLambdaTaskResults(this.triggerFirstTimeAeP2p_10DaysBeforeDate)
  }
  
  prepareUserWhoCreatedEventForSendingEmails(searchInEventCoordinator = [], searchForSuitableInCreatedEvents = []) {
    if (isEmpty(searchInEventCoordinator) || isEmpty(searchForSuitableInCreatedEvents)) return []
    
    const array = []
    searchInEventCoordinator.map(function(i) {
      searchForSuitableInCreatedEvents.map(function(y) {
        if (i.regEvent.regEventID == y.regEvent.regEventID) {
          let includesCreatedPersonIDs = [i.regEvent.GeneralContactPersonID, i.regEvent.ContactPersonID].includes(y.Object.CreatedPersonID)
          if (!includesCreatedPersonIDs) {
            array.push(y)
          }
        }
      })
    })
    return array
  }
}

module.exports = FirstTimeAeP2pService