const Component = require("component").Component
const FirstTimeAeP2pFactory = require('./first-time-ae-p2p.factory')

const service = FirstTimeAeP2pFactory.service()

const FirstTimeAeP2pComponent = Component(function ({ router }) {
  router.get('/', first_time_ae_p2p)
})

function* first_time_ae_p2p() {
  const results = yield service.runTriggerTenDaysBeforeDate()
  this.ok({...results})
}

module.exports = { FirstTimeAeP2pComponent }