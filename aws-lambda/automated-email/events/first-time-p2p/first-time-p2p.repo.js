const { LegacyDB } = require('ms-shared/legacy-db')
const OldLegacyDB = require("ms-shared/old-legacy-db")
const { EventQuery } = require('../mixin_query')

class FirstTimeP2pRepo {
  
  /**
   * Create AwsLambdaTaskResultsAutomatedEmail object
   * @param {Object} dto - The value object.
   * @return {Promise} Promise object represents the response.
   */
  async create(dto) {
    return LegacyDB.knex('AwsLambdaTaskResultsAutomatedEmail')
      .insert({...dto})
  }
  
  /**
   *
   * @param {Object} options - The value object
   * @param triggerType - The value object
   * @returns {Promise<*>}
   */
  async getAllByTriggerTypeFromAwsLambdaTaskResults(options = {}, triggerType = {}) {
    const { startDate, endDate } = options
    
    return LegacyDB.knex('AwsLambdaTaskResultsAutomatedEmail')
      .select('BulbasaurID', 'createdAt')
      .distinct('BulbasaurID')
      .where({ triggerType, status: 1 })
      .whereNotNull('BulbasaurID')
      .whereBetween(OldLegacyDB.raw('createdAt'), [startDate, endDate])
  }
  
  async getEventsDataByUserWhoMarkedSetupComplete() {
    let createdEventsCount = OldLegacyDB('Bulbasaur')
      .options({ nestTables: true })
      .use(EventQuery)
      .select(
        'Bulbasaur.ID AS BulbasaurID',
        'BulbasaurTracker.SetupCompletedPersonID AS SetupCompletedPersonID',
        'Bulbasaur.GeneralContactPersonID as GeneralContactPersonID',
        'Bulbasaur.ContactPersonID as ContactPersonID',
        'Bulbasaur.Title AS eventTitle',
        'Person.FirstName AS eventContactFirst',
        'Person.LastName AS eventContactLast',
        'Email.Email AS eventContactEmail'
      )
      .join('Charmander', function () {
        this.on('Bulbasaur.ObjectID', '=', 'Charmander.ID')
          .andOn('Charmander.Committed', '=', 1)
          .andOn('Charmander.Deleted', '=', 0)
      })
      .join('Owner', 'Owner.ID', 'Charmander.OwnerID')
      .leftJoin('Charmander', 'Bulbasaur.ID', 'Charmander.BulbasaurID')
      .leftJoin('CharmanderType', 'Charmander.CharmanderTypeID', 'CharmanderType.ID')
      .leftJoin('BulbasaurTracker', 'Bulbasaur.ID', 'BulbasaurTracker.BulbasaurID')
      .leftJoin('Person', 'BulbasaurTracker.SetupCompletedPersonID', 'Person.ID')
      .leftJoin('Email', function () {
        this.on('Person.ID', '=', 'Email.PersonID')
          .andOn('Email.DefaultEmail', '=', 1)
      })
      .required_p2p_bill_event_type()
      .is_event_setup_complete_p2p()
      .whereNotNull('Email.Email')
      .where('Email.Email', '!=', '')
      .groupBy('Bulbasaur.ID')
      .as('Temp')
  
    return LegacyDB.knex.queryBuilder()
      .select('*')
      .count('* AS CreatedEventsCount')
      .from(createdEventsCount)
      .groupBy("Temp.SetupCompletedPersonID")
  }
  
  async getEventsAsEventCoordinator(options = {}, include = {}) {
    const { startDate, endDate } = options
    const { includeEvents = [] } = include
  
    return OldLegacyDB('Bulbasaur')
      .options({ nestTables: true })
      .use(EventQuery)
      .select(
        'Bulbasaur.ID AS BulbasaurID',
        'Bulbasaur.GeneralContactPersonID AS GeneralContactPersonID',
        'Bulbasaur.ContactPersonID AS ContactPersonID',
        'Bulbasaur.Title AS eventTitle'
      )
      .with_active_object({ for_table: 'Bulbasaur' })
      .with_object_owner()
      .with_billing_and_product()
      .with_person_profile_for_coordinator()
      .with_default_contact_email_for_coordinator()
      .with_reg_event_tracker()
      .where_no_empty_email()
      .include_events(includeEvents)
      .is_event_setup_complete_p2p()
      .whereBetween(OldLegacyDB.raw('BulbasaurTracker.updatedAt'), [startDate, endDate])
  }
  
  async getEventsAsEventContacts(include = {}) {
    const { includeEvents = [], includePersonIds = [] } = include
    return OldLegacyDB('Bulbasaur')
      .options({ nestTables: true })
      .use(EventQuery)
      .select(
        'Bulbasaur.ID AS BulbasaurID',
        'Bulbasaur.GeneralContactPersonID AS GeneralContactPersonID',
        'Bulbasaur.ContactPersonID AS ContactPersonID',
        'Bulbasaur.Title AS eventTitle'
      )
      .with_active_object({ for_table: 'Bulbasaur' })
      .with_object_owner()
      .with_billing_and_product()
      .with_person_profile_for_contacts()
      .with_default_contact_email_for_contacts()
      .with_reg_event_tracker()
      .where_no_empty_email()
      .include_events(includeEvents)
      .for_person(includePersonIds)
      .is_event_setup_complete_p2p()
  }
  
}

module.exports = FirstTimeP2pRepo