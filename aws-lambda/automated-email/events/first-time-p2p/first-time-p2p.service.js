const { triggers_by_resource_type } = require('../../trigger_types')
const { FirstTimeP2P_2d_AfterSetupComplete } = triggers_by_resource_type.event.choices
const {
  currentDate, subtractDate, addDateToGiven, isEmpty, prepareEventContactsIds
} = require('../../utils')

const EmailNotificationFactory = require('../../../notifications/email/first-time-p2p/email-notification.factory')

class FirstTimeP2pService {
  
  /**
   * @constructs FirstTimeAeP2pService
   * @param repo
   */
  constructor(repo) {
    this._repo = repo
    this.trigger = FirstTimeP2P_2d_AfterSetupComplete.key.trim()
  }
  
  /**
   * insert data in table
   * @param results
   * @returns {Promise<unknown[]>}
   */
  async insertData(results) {
    return Promise.all(
      results.map(async (data) => {
        if (data) {
          const preparedData = this.prepareDataForInsertData(data)
          await this._repo.create(preparedData)
        }
      })
    );
  }
  
  /**
   * prepare data for inserting data into a table
   * @param data
   * @returns {{transmissionID: *, createdAt: *, triggerType: (*|string), regEventID: *, status: number}}
   */
  prepareDataForInsertData(data) {
    const { transmissionID, regEventID } = data
    return {
      triggerType: this.trigger,
      transmissionID: transmissionID,
      regEventID: regEventID,
      status: 1,
      createdAt: currentDate()
    }
  }
  
  /**
   *
   * @param factoryRequest
   * @returns {Promise<boolean|*>}
   */
  async sendEmail(factoryRequest) {
    const emailProvider = EmailNotificationFactory.command(factoryRequest);
    return emailProvider.sendEmail();
  }
  
  /**
   * start sending Emails
   * @param eventsData
   * @returns {Promise<unknown[]>}
   */
  async startSendingEmails(eventsData) {
    return Promise.all(
      eventsData.map(async data => this.sendEmail({ action: 'two-days-passed', data } ))
    )
  }
  
  async runTrigger() {
    const AwsLambdaData = await this.getAwsLambdaDataFromTable()
    const excludeAwsLambdaEvents = this.getExcludedAwsLambdaEvents(AwsLambdaData)
    
    //user who marked the setup as complete
    const eventsDataByUserWhoMarkedSetupComplete = await this.getEventsDataByUserWhoMarkedSetupComplete()
    const firstEvent = this.getFirstEventIfExists(eventsDataByUserWhoMarkedSetupComplete, excludeAwsLambdaEvents)
    
    if (isEmpty(firstEvent)) {
      return this.createMessageToResponseIfEmptyData(AwsLambdaData)
    }
  
    //contact_person
    const eventsDataAsEventCoordinator = await this.getEventsDataAsEventCoordinator(
      { includeEvents: firstEvent.map((x) => ( x.regEvent )) }
      )
    
    //coordinator
    const eventsDataAsEventContacts = await this.getEventsDataAsEventContacts(
      eventsDataAsEventCoordinator,
      { includeEvents: eventsDataAsEventCoordinator.map((x) => ( x.regEvent )) }
    )
  
    //ready array 'user who marked the setup as complete'
    const preparedUserWhoCreated = this.prepareUserWhoCreatedEventForSendingEmails(eventsDataAsEventCoordinator, firstEvent)
    const combineEventsData = eventsDataAsEventCoordinator.concat(eventsDataAsEventContacts, preparedUserWhoCreated)
  
    const mailingResult = await this.startSendingEmails(combineEventsData)
    await this.insertData(mailingResult)
  
    return {
      message: isEmpty(mailingResult) ? 'No data for mailing' : 'Sending Emails was successful',
      data: mailingResult
    }
  }
  
  /**
   * Create a message to reply if there is no data for mailing
   * @param eventsData
   * @param AwsLambdaData
   * @returns {boolean|{message: string}}
   */
  createMessageToResponseIfEmptyData(AwsLambdaData) {
    const eventsDataMess = "1. Get first event if exists: [empty];"
    const awsLambdaDataMess = !isEmpty(AwsLambdaData)
      ? `2. Emails have already been sent before. Check the table 'AwsLambdaTaskResultsAutomatedEmail', triggerType = ${this.trigger}`
      : ""
    
    return {
      message: `${eventsDataMess} ${awsLambdaDataMess}`
    }
  }
  
  prepareUserWhoCreatedEventForSendingEmails(searchInEventCoordinator = [], searchForSuitableInCreatedEvents = []) {
    if (isEmpty(searchInEventCoordinator) || isEmpty(searchForSuitableInCreatedEvents)) return []
    
    const array = []
    searchInEventCoordinator.map(function(i) {
      searchForSuitableInCreatedEvents.map(function(y) {
        if (i.regEvent.regEventID == y.regEvent.regEventID) {
          let includesCreatedPersonIDs =
            [i.regEvent.GeneralContactPersonID, i.regEvent.ContactPersonID].includes(y.regEventTracker.SetupCompletedPersonID)
          if (!includesCreatedPersonIDs) {
            array.push(y)
          }
        }
      })
    })
    return array
  }
  
  /**
   * get events and related data for the Event Contacts
   * @param eventCoordinator
   * @returns {Promise<Array|*>}
   */
  async getEventsDataAsEventContacts(eventCoordinator = [], include = {}) {
    if (isEmpty(eventCoordinator)) return []
    
    const generalContactPersonIDs = prepareEventContactsIds(eventCoordinator)
    
    if (!isEmpty(generalContactPersonIDs)) {
      include.includePersonIds = generalContactPersonIDs
      return this._repo.getEventsAsEventContacts(include)
    }
    return []
  }
  
  /**
   * get events and related data for the Event Coordinator
   * @param exclude - exclude events that are stored in the table: AwsLambdaTaskResultsAutomatedEmail
   * @returns {Promise<*>}
   * 2 days have passed since P2P Event Setup Complete
   */
  async getEventsDataAsEventCoordinator(include = {}) {
    const timeOptions = {
      daysPassedInHours: 48,
      timeIntervalInHours: 3
    };
    const startDate = subtractDate(timeOptions.daysPassedInHours + timeOptions.timeIntervalInHours, 'hours');
    const endDate = addDateToGiven(startDate, timeOptions.timeIntervalInHours, 'hours');
    
    return this._repo.getEventsAsEventCoordinator({ startDate, endDate }, include)
  }
  
  getFirstEventIfExists(events = [], excludeEvents = []) {
    return this.prepareCreatedEvents(events, excludeEvents)
  }
  
  prepareCreatedEvents(events = [], excludeEvents = []) {
    if (isEmpty(events)) return []
    let array = [];
    
    events.map(function (data) {
      const {
        CreatedEventsCount = '', eventContactEmail, eventContactFirst, eventContactLast, eventTitle, regEventID,
        GeneralContactPersonID, ContactPersonID, SetupCompletedPersonID
      } = data
      if (CreatedEventsCount == 1 && !excludeEvents.includes(regEventID)) {
        const prepare = {
          regEvent: { eventTitle, regEventID, GeneralContactPersonID, ContactPersonID },
          regEventTracker: { SetupCompletedPersonID },
          Person: { eventContactFirst, eventContactLast},
          Email: { eventContactEmail },
        }
        array.push(prepare)
      }
    })
    return array
  }
  
  /**
   * get user who marked the setup as complete
   * @returns {Promise<*>}
   */
  async getEventsDataByUserWhoMarkedSetupComplete() {
    return this._repo.getEventsDataByUserWhoMarkedSetupComplete()
  }
  
  getExcludedAwsLambdaEvents(events = []) {
    if (isEmpty(events)) return []
    return events.map(a => a.regEventID)
  }
  
  
  /**
   * get data from table AwsLambdaTaskResultsAutomatedEmail
   * @returns {Promise<*>}
   */
  async getAwsLambdaDataFromTable() {
    const startDate = subtractDate(4, 'hours');
    const endDate = currentDate();
    return this._repo.getAllByTriggerTypeFromAwsLambdaTaskResults({ startDate, endDate }, this.trigger)
  }
}

module.exports = FirstTimeP2pService