const FirstTimeP2pService = require('./first-time-p2p.service')
const FirstTimeP2pRepo = require('./first-time-p2p.repo')

class FirstTimeP2pFactory {
  service(options = {}) {
    return new FirstTimeP2pService(this.repo())
  }
  
  repo(options = {}) {
    return new FirstTimeP2pRepo()
  }
}

module.exports = new FirstTimeP2pFactory()