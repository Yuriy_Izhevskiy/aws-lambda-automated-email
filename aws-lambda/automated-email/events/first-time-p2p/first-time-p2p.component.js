const Component = require("component").Component
const FirstTimeP2pFactory = require('./first-time-p2p.factory')

const service = FirstTimeP2pFactory.service()

const FirstTimeP2pComponent = Component(function ({ router }) {
  router.get('/', first_time_p2p)
})

function* first_time_p2p() {
  let res = yield service.runTrigger()
  this.ok({...res})
}

module.exports = { FirstTimeP2pComponent }