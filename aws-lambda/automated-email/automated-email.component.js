const Component = require('component').Component

const { EventsComponent } = require('./events/events.component')
const { NewAccountComponent } = require('./new-account/new-account.component')
const { UsersComponent } = require('./users/users.component')

const AutomatedEmailComponent = Component(function ({ router }) {
  router.use('/events', EventsComponent.routes())
  router.use('/new-account', NewAccountComponent.routes())
  router.use('/users', UsersComponent.routes())
});

module.exports = { AutomatedEmailComponent }