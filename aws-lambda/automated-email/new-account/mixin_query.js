const Query = require("query")
const { isEmpty } = require('../utils')

const AccountQuery = Query({
  exclude_owners(exclude = []) {
    if (!isEmpty(exclude)) {
      this.whereNotIn('Owner.ID', exclude.map(a => a.ownerID))
    }
  },
})

module.exports = {
  AccountQuery
}