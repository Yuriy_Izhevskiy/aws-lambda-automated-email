const Component = require('component').Component

const { NoEventCreatedComponent } = require('./no-event-created/no-event-created.component')

const NewAccountComponent = Component(function ({ router }) {
  router.use('/no-event-created', NoEventCreatedComponent.routes())
});

module.exports = { NewAccountComponent }

