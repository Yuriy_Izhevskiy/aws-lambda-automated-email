const Component = require('component').Component

const { XDaysBaseComponent } = require('./x-days/x-days.base.component')

const NoEventCreatedComponent = Component(function ({ router }) {
  router.use(XDaysBaseComponent.routes())
})

module.exports = { NoEventCreatedComponent }