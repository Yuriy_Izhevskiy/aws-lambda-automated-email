const OneDeyService = require('./services/one-dey.service')
const TenDeyService = require('./services/ten-days.service')
const ThreeDeyService = require('./services/three-days.service')

const XDaysBaseService = require('./x-days.base.service')
const XDaysBaseRepo = require('./x-days.base.repo')

class XDaysBaseFactory {
  service(instance = '') {
    switch (instance.trim()) {
      case 'one-day':
        return new OneDeyService(
          new XDaysBaseService(this.repo())
        );
      case 'ten-days':
        return new TenDeyService(
          new XDaysBaseService(this.repo())
        );
      case 'three-days':
        return new ThreeDeyService(
          new XDaysBaseService(this.repo())
        );
      default:
        throw new Error(`Unknown XDaysBase instance - "${instance}".`);
    }
  }
  
  repo(options = {}) {
    return new XDaysBaseRepo()
  }
}


module.exports = new XDaysBaseFactory()