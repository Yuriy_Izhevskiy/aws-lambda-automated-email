const Component = require('component').Component
const XDaysBaseFactory = require('./x-days.base.factory')

const XDaysBaseComponent = Component(function ({ router }) {
  router.get('/one-day', one_day)
  router.get('/three-days', three_days)
  router.get('/ten-days', ten_days)
})

function* one_day() {
  const results = yield XDaysBaseFactory.service('one-day').runTrigger()
  this.ok({...results, 'one-day': 'one-day'})
}

function* three_days() {
  const results = yield XDaysBaseFactory.service('three-days').runTrigger()
  this.ok({...results, 'three-days': 'three-days'})
}

function* ten_days() {
  const results = yield XDaysBaseFactory.service('ten-days').runTrigger()
  this.ok({...results, 'ten-days': 'ten-days'})
}

module.exports = { XDaysBaseComponent }