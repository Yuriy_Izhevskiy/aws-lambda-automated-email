const {
  currentDate, subtractDate, addDateToGiven, isEmpty, onlyUnique
} = require('../../../utils')
const EmailNotificationFactory = require('../../../../notifications/email/new-account/no-event-created/email-notification.factory')

class XDaysBaseService {
  constructor(repo) {
    this.trigger = ''
    this._repo = repo
  }
  
  runTrigger() {
    throw new Error(
      "XDaysBaseService it`s abstract class. You must override runTrigger method"
    );
  }
  
  /**
   * insert data in table
   * @param results
   * @returns {Promise<unknown[]>}
   */
  async insertData(results) {
    return Promise.all(
      results.map(async (data) => {
        if (data) {
          const preparedData = this.prepareDataForInsertData(data)
          await this._repo.create(preparedData)
        }
      })
    );
  }
  
  /**
   * prepare data for inserting data into a table
   * @param data
   * @returns {{transmissionID: *, createdAt: *, triggerType: (*|string), regEventID: *, status: number}}
   */
  prepareDataForInsertData(data) {
    const { transmissionID, ownerID } = data
    return {
      triggerType: this.trigger,
      transmissionID: transmissionID,
      ownerID: ownerID,
      status: 1,
      createdAt: currentDate()
    }
  }
  
  /**
   * get data from table AwsLambdaTaskResultsAutomatedEmail
   * @returns {Promise<*>}
   */
  async getAwsLambdaDataFromTable() {
    const startDate = subtractDate(3, 'hours');
    const endDate = currentDate();
    
    return this._repo.getAllByTriggerTypeFromAwsLambdaTaskResults({ startDate, endDate }, this.trigger)
  }
  
  async getUserDataWhoCreatedAccount(timeOptions = {}, exclude = []) {
    const { daysPassedInHours, timeIntervalInHours } = timeOptions
    const startDate = subtractDate(daysPassedInHours + timeIntervalInHours, 'hours');//2021-03-05 07:00:54
    const endDate = addDateToGiven(startDate, timeIntervalInHours, 'hours');//2021-03-05 10:20:54
  
    return this._repo.getUserWhoCreatedAccount({ startDate, endDate }, exclude)
  }
  
  /**
   * get existing account events by owner id
   * @param ownerIds
   * @returns {Promise<*>}
   */
  async getExistingAccountEvents(ownerIds = []) {
    return this._repo.getExistingAccountEvents(ownerIds)
  }
  
  /**
   * Create a message to reply if there is no data for mailing
   * @param eventsData
   * @param AwsLambdaData
   * @returns {boolean|{message: string}}
   */
  createMessageToResponseIfEmptyData(AwsLambdaData) {
    const eventsDataMess = "1. Get the data of the user who created the account: [empty];"
    const awsLambdaDataMess = !isEmpty(AwsLambdaData)
      ? `2. Emails have already been sent before. Check the table 'AwsLambdaTaskResultsAutomatedEmail', triggerType = ${this.trigger}`
      : ""
    
    return {
      message: `${eventsDataMess} ${awsLambdaDataMess}`
    }
  }
  
  prepareOwnerIds(ownerData = []) {
    if (isEmpty(ownerData)) return []
    return ownerData.map(function ({Owner}) {
      let { ownerID } = Owner
      return ownerID
    })
  }
  
  /**
   * Prepare data for emails
   * @param userDataWhoCreatedAccount
   * @param existingAccountEvents
   * @returns {[]}
   */
  prepareDataForEmails({ userDataWhoCreatedAccount = [], existingAccountEvents = [] }) {
    let results = this.differenceOf2Arrays(
      this.getOwnerIdsFromExistingAccountEvents(existingAccountEvents),
      this.getOwnersFromAccountEvents(userDataWhoCreatedAccount)
    )
  
    return this.dataForEmails(userDataWhoCreatedAccount, onlyUnique(results))
  }
  
  
  dataForEmails(fullArrayAccountData, uniqueArrayAccountData) {
    const dataForEmails = []
    for(var i = 0; i < fullArrayAccountData.length; i++) {
      for(var y = 0; y < uniqueArrayAccountData.length; y++) {
        if (fullArrayAccountData[i].Owner.ownerID == uniqueArrayAccountData[y]) {
          dataForEmails.push(fullArrayAccountData[i])
        }
      }
    }
  
    return dataForEmails
  }
  
  differenceOf2Arrays (array1 = [], array2 = []) {
    const store = [];
    const length = array1.length >= array2.length ? array1.length : array2.length;
    
    array1 = array1.toString().split(',').map(Number);
    array2 = array2.toString().split(',').map(Number);
    
    for (let i = 0; i <= length; i++) {
      if (array1.indexOf(array2[i]) === -1 && array2[i] !== undefined) { store.push(array2[i]) }
      if (array2.indexOf(array1[i]) === -1 && array1[i] !== undefined) { store.push(array1[i]) }
    }
    
    return store.sort((a,b) => a - b);
  }
  
  /**
   * Get all owners ids from an array of account events
   * @param data
   * @returns {*}
   */
  getOwnersFromAccountEvents(data) {
    return data.map((x) => ( x.Owner.ownerID ));
  }
  
  /**
   * Get owner ids from existing account events
   * @param data
   * @returns {*}
   */
  getOwnerIdsFromExistingAccountEvents(data) {
    return data.map((x) => ( x.OwnerID ));
  }
  
  async startSendingEmails(dataArray, action = '') {
    return Promise.all(
      dataArray.map(async data => this.sendEmail({ action, data } ))
    )
  }
  
  async sendEmail(factoryRequest) {
    const data = this.prepareDataForSendEmail(factoryRequest);
    const emailProvider = EmailNotificationFactory.command(data);
    return emailProvider.sendEmail();
  }
  
  /**
   * @param factoryRequest
   * @returns {{action: *, data: *}}
   */
  prepareDataForSendEmail(factoryRequest) {
    const { action, data } = factoryRequest;
  
    return {
      ...(data !== undefined && { data }),
      ...(action !== undefined && { action }),
    };
  }
  
  
}

module.exports = XDaysBaseService