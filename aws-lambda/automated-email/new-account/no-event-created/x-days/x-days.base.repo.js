const { LegacyDB } = require('ms-shared/legacy-db')
const OldLegacyDB = require("ms-shared/old-legacy-db")
const { AccountQuery } = require('../../../new-account/mixin_query')


/**
 * Class representing a XDaysBaseRepo
 * @class
 */
class XDaysBaseRepo {
  
  /**
   * Create AwsLambdaTaskResultsAutomatedEmail object
   * @param {Object} dto - The value object.
   * @return {Promise} Promise object represents the response.
   */
  async create(dto) {
    return LegacyDB.knex('AwsLambdaTaskResultsAutomatedEmail')
      .insert({...dto})
  }
  
  /**
   *
   * @param {Object} options - The value object
   * @param triggerType - The value object
   * @returns {Promise<*>}
   */
  async getAllByTriggerTypeFromAwsLambdaTaskResults(options = {}, triggerType = {}) {
    const { startDate, endDate } = options
    
    return LegacyDB.knex('AwsLambdaTaskResultsAutomatedEmail')
      .select('BulbasaurID', 'createdAt', 'ownerID')
      .where({ triggerType, status: 1 })
      .whereBetween(OldLegacyDB.raw('createdAt'), [startDate, endDate])
  }
  
  async getUserWhoCreatedAccount(options = {}, excludeOwners = []) {
    const { startDate, endDate } = options
    const role = {
      OwnerAdmin: 19
    }
  
    return OldLegacyDB('Owner')
      .options({ nestTables: true })
      .use(AccountQuery)
      .select(
        'PokemonCity.ID AS userID',
        'Owner.ID AS ownerID',
        'Charmander.OwnerID AS objectOwnerID',
        'Charmander.CreatedPersonID AS createdPersonID',
        'Charmander.CreatedDate AS createdDateOwner',
        'Person.FirstName AS eventContactFirst',
        'Person.LastName AS eventContactLast',
        'Email.Email AS eventContactEmail'
      )
      .join('Charmander', function () {
        this.on('Charmander.ID', '=', 'Owner.ObjectID')
          .andOn('Charmander.Committed', '=', 1)
          .andOn('Charmander.Deleted', '=', 0)
      })
      .leftJoin('Person', function () {
        this.on('Person.ID', '=', 'Charmander.CreatedPersonID')
          .andOn('Person.IsPublic', '=', 0)
      })
      .join('PokemonCity', function () {
        this.on('PokemonCity.PersonID', '=', 'Charmander.CreatedPersonID')
          .andOn('PokemonCity.IsPublic', '=', 0)
      })
      .leftJoin('UserRoleMap', function () {
        this.on('UserRoleMap.UserID', '=', 'PokemonCity.ID')
      })
      .leftJoin('Email', function () {
        this.on('Person.ID', '=', 'Email.PersonID')
          .andOn('Email.DefaultEmail', '=', 1)
      })
      .whereIn('UserRoleMap.RoleID', Object.values(role))
      .exclude_owners(excludeOwners)
      .whereBetween(OldLegacyDB.raw('Charmander.CreatedDate'), [startDate, endDate])
  }
  
  async getExistingAccountEvents(ownerIds = []) {
    return OldLegacyDB('Bulbasaur')
      .select([
        'Owner.ID as OwnerID',
        'Bulbasaur.ID',
        'Bulbasaur.Title',
        'Bulbasaur.IsActive'
      ])
    
      .with_active_object()
    
      .join('Owner', 'Owner.ID', 'Charmander.OwnerID')
      .select('Owner.ID as OwnerID', 'Owner.Name ')
      
      .leftJoin('Charmander', 'Bulbasaur.ID', 'Charmander.BulbasaurID')
      .select('Charmander.CharmanderTypeID', 'Charmander.ID', 'Charmander.OwnerContractCommitmentID')
    
      .select('CharmanderType.ID', 'CharmanderType.CharmanderProductID')
      .leftJoin('CharmanderType', 'Charmander.CharmanderTypeID', 'CharmanderType.ID')
    
      .select('regType.IsCampaign', 'regType.HasHotelCapacity')
      .join('regType', 'Bulbasaur.regTypeID', 'regType.ID')
      
      .select('BulbasaurDisplay.EventDate')
      .leftJoin('BulbasaurDisplay', 'Bulbasaur.ID', 'BulbasaurDisplay.EventID')
      .where('BulbasaurDisplay.IsStartDate', 1)
      .min('BulbasaurDisplay.EventDate as StartDate')
      .groupBy('Owner.ID')
    
      .whereNull('Bulbasaur.ParentEventID')
      .whereIn('Charmander.OwnerID', ownerIds)
  }
  
}

module.exports = XDaysBaseRepo