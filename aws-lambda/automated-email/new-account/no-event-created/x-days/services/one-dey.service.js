const { triggers_by_resource_type } = require('../../../../trigger_types')
const { NewAccount_NoEventCreated_1Day } = triggers_by_resource_type.event.choices
const { isEmpty } = require('../../../../utils')

/**
 * Class representing a OneDeyService
 * @class
 */
class OneDeyService {
  constructor(xDaysBaseService) {
    this._xDaysBaseService = xDaysBaseService
    this._xDaysBaseService.trigger = NewAccount_NoEventCreated_1Day.key.trim()
  }
  
  async runTrigger() {
    const AwsLambdaData = await this._xDaysBaseService.getAwsLambdaDataFromTable()
    
    const userDataWhoCreatedAccount = await this._xDaysBaseService.getUserDataWhoCreatedAccount(
      {
        timeIntervalInHours: 3,
        daysPassedInHours: 24
      },
      AwsLambdaData)
    
    if (isEmpty(userDataWhoCreatedAccount)) {
      return this._xDaysBaseService.createMessageToResponseIfEmptyData(AwsLambdaData)
    }

    const ownerIds = await this._xDaysBaseService.prepareOwnerIds(userDataWhoCreatedAccount)
    const existingAccountEvents = await this._xDaysBaseService.getExistingAccountEvents(ownerIds)
    
    const preparedDataForEmail = this._xDaysBaseService.prepareDataForEmails({ userDataWhoCreatedAccount, existingAccountEvents})
    const mailingResult = await this._xDaysBaseService.startSendingEmails(preparedDataForEmail, 'one-day-passed')
  
    await this._xDaysBaseService.insertData(mailingResult)
  
    return {
      message: isEmpty(mailingResult) ? 'No data for mailing' : 'Sending Emails was successful',
      data: mailingResult
    }
  }
}

module.exports = OneDeyService