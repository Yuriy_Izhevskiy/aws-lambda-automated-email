const { ServerConfig } = require("config")
const SECRET = ServerConfig.aws_lambda_access_key

function awsLambda_authenticate({from_param} = {}) {
  return function* (next) {
    const accessKey = this.get('Aws-Lambda-Access-Key')
    if (!accessKey) yield this.throw({ status: 401 })
    if (accessKey !== SECRET) yield this.throw({ status: 401 })
  
    this.state.user = 'awsLambda'
    yield next
  }
}

module.exports = awsLambda_authenticate